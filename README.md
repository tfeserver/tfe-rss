# Tfe RSS

Rss Client for FirefoxOS (aka B2G), that connects to famous APIs like TheOldReader, Feedly, Tiny Tiny RSS or Owncloud.

Api support:
- The old reader
- Tiny Tiny RSS
- Owncloud
- Feedly

Features:
- Read/Unread item
- Star/Unstar item
- Like/Unlike if api support it
- Open link in the browser
- Auto update and notifications
- Share link
- Advanced Features / regex filter


![feedly login](http://tfeserver.be/dl/tfe_rss_screenshots/feedly_login.png)

![TT-RSS Login](http://tfeserver.be/dl/tfe_rss_screenshots/tt-rss_login.png)

![Feedly list](http://tfeserver.be/dl/tfe_rss_screenshots/list.png)

![Opened item](http://tfeserver.be/dl/tfe_rss_screenshots/big.png)